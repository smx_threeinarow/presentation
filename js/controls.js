window.addEventListener("load", () => {
    const webSocket = new WebSocket("ws://localhost:3333")

    webSocket.onopen = (_) => {
        console.log("Controls are enabled")
    }

    webSocket.onmessage = (event) => {
        console.log("Message received", event.data)
    }

    document.querySelectorAll("button").forEach(x => {
        x.addEventListener("click", () => {
            const action = x.dataset["action"]
            console.log("Action send:", action)
            webSocket.send(`action: ${action}`)
        })
    })
})

