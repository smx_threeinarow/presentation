window.addEventListener("load", () => {
    const webSocket = new WebSocket("ws://localhost:3333")

    webSocket.onopen = (_) => {
        webSocket.send("presentation: start")
        console.log("Presentation Started")
    }

    webSocket.onmessage = (event) => {
        console.log("Message received", event.data)
        if (event.data.startsWith("move: ")) {
            const move = event.data.substring(6)
            if (move === "up") {
                Reveal.up()
            }
            else if (move === "down") {
                Reveal.down()
            }
            else if (move === "left") {
                Reveal.left()
            }
            else if (move === "right") {
                Reveal.right()
            }
            else {
                alert(move)
            }
        }
    }
})
