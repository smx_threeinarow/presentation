const std = @import("std");
const websocket = @import("websocket.zig/src/websocket.zig");
const Conn = websocket.Conn;
const Message = websocket.Message;
const Handshake = websocket.Handshake;

// Define a struct for "global" data passed into your websocket handler
const Context = struct { presentations: std.ArrayList(*Handler) };

pub fn main() !void {
    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = general_purpose_allocator.allocator();
    const presentations_allocator = general_purpose_allocator.allocator();

    // this is the instance of your "global" struct to pass into your handlers
    var context = Context{ .presentations = std.ArrayList(*Handler).init(presentations_allocator) };

    const address = "127.0.0.1";
    const port = 3333;
    std.debug.print("Listening on addresss {s}:{}\n", .{ address, port });
    try websocket.listen(Handler, allocator, &context, .{
        .port = port,
        .max_headers = 10,
        .address = address,
    });
}

const Handler = struct {
    conn: *Conn,
    context: *Context,
    is_presentation: bool,
    index: usize,

    pub fn init(h: Handshake, conn: *Conn, context: *Context) !Handler {
        // `h` contains the initial websocket "handshake" request
        // It can be used to apply application-specific logic to verify / allow
        // the connection (e.g. valid url, query string parameters, or headers)

        _ = h; // we're not using this in our simple case

        return Handler{
            .conn = conn,
            .context = context,
            .is_presentation = false,
            .index = 0,
        };
    }

    // optional hook that, if present, will be called after initialization is complete
    // pub fn afterInit(self: *Handler) !void {}

    pub fn handle(self: *Handler, message: Message) !void {
        const data = message.data;

        std.debug.print("NEW DATA FROM {d}. DATA: {s}\n", .{ self.index, data });

        if (std.mem.startsWith(u8, data, "action: ")) {
            const allocator = std.heap.page_allocator;
            for (self.context.presentations.items) |item| {
                std.debug.print("SEND DATA TO {d}. DATA {s}\n", .{ item.index, data });
                try item.conn.write(std.fmt.allocPrint(allocator, "move: {s}", .{data[8..]}) catch "move: err");
            }
        } else if (std.mem.eql(u8, data, "presentation: start")) {
            if (self.context.presentations.append(self)) {
                self.index = self.context.presentations.items.len - 1;
                self.is_presentation = true;
            } else |_| {}
        }
    }

    // called whenever the connection is closed, can do some cleanup in here
    pub fn close(self: *Handler) void {
        if (self.is_presentation) {
            const removed = self.context.presentations.orderedRemove(self.index);
            var index = removed.index;
            while (index < self.context.presentations.items.len) {
                self.context.presentations.items[index].index = self.context.presentations.items[index].index - 1;
                index = index + 1;
            }
        }
    }
};
